use std::result;
use super::error::Error;

/// A type alias for `Result<T, Error>`.
pub type Result<T> = result::Result<T, Error>;