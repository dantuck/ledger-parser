// use std::{
//     fs::File,
//     io::{self, prelude::*}
// };

// use super::super::result::Result;


// pub struct BufReader {
//     reader: io::BufReader<File>,
// }

// impl BufReader {
//     pub fn open(path: impl AsRef<std::path::Path>) -> Result<Self> {
//         let file = File::open(path)?;
//         let reader = io::BufReader::new(file);

//         // // Read the file line by line using the lines() iterator from std::io::BufRead.
//         // for (index, line) in reader.lines().enumerate() {
//         //     let line = line.unwrap(); // Ignore errors.
//         //     // Show the line and its number.
//         //     println!("{}. {}", index + 1, line);
//         // }

//         Ok(Self { reader })
//     }

//     // pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
//     // where P: AsRef<std::path::Path>, {
//     //     let file = File::open(filename)?;

//     //     let reader = io::BufReader::new(file);

//     //     // Ok(io::BufReader::new(file).lines())
//     // }

//     pub fn read_line<'buf>(
//         &mut self,
//         buffer: &'buf mut String,
//     ) -> Option<io::Result<&'buf mut String>> {
//         buffer.clear();

//         self.reader
//             .read_line(buffer)
//             .map(|u| if u == 0 { None } else { Some(buffer) })
//             .transpose()
//     }

//     // pub fn read_line<'buf>(
//     //     &mut self,
//     //     buffer: &'buf mut String,
//     // ) -> Option<Result<&'buf mut String>> {
//     //     buffer.clear();

//     //     match self.reader
//     //         .read_line(buffer)
//     //         .map(|u| if u == 0 { None } else { Some(buffer) })
//     //         .transpose() {
//     //             Some(Ok(value)) => Some(Ok(value)),
//     //              _ => None
//     //         }
//     // }
// }