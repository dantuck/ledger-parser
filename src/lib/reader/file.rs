use std::{fs};
use super::super::result::Result;
use super::super::error::{
    Error,
    ErrorKind
};

pub struct File {
    pub content: String,
}

impl File {
    pub fn load(path: &str) -> Result<Self> {
        let path = std::path::Path::new(path);
        match fs::read_to_string(path) {
            Err(err) => return Err(Error::new(
                ErrorKind::Io(err, Some(
                    format!(
                        "While parsing file \"{}\"",
                        path.display(),
                    ),
                )),
                None,
            )),
            Ok(content) => Ok(Self { content })
        }
    } 
}