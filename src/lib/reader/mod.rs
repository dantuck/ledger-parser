pub mod file;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn buffer_reader_reads_first_line() {
        let file = file::File::load("examples/sample1.ledger");

        assert_eq!(file.is_ok(), true);

        if let Ok(file) = file {
            let line = file.content.lines().next();
            assert_eq!(line.unwrap().trim(), "2010/12/01 * Checking balance")
        }
    }
}
