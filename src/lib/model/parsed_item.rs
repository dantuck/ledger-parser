use super::post::Post;

#[derive(Debug)]
pub enum ParsedItem {
	Transaction {
		date: String,
		description: String,
		posts: Vec<Post>,
	},
}
