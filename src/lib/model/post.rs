use super::amount::Amount;

#[derive(Debug)]
pub enum Post {
	UnbalancedPost {
		account: String,
		unbalanced_amount: Option<Amount>,
		balance_assertion: Option<Amount>,
	},
}
