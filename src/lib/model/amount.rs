#[derive(Debug, Clone)]
pub struct Amount {
	pub value: f64,
    pub account: String,
}