use super::Parser;

pub(super) fn runner(parser: &mut Parser) {
    while let Some(c) = parser.line_characters.get(parser.line_position) {
		if !c.is_whitespace() {
			while let Some(c) = parser.line_characters.get(parser.line_position) {
				if !c.is_whitespace() {
					break;
				}
				parser.line_position += 1;
			}
		}

		// try_consume_string(&mut parser, &);

		parser.line_position += 1;
	}
}



pub(super) fn try_consume_string(parser: &mut Parser, str: &str) -> bool {
	let mut pos = parser.line_position;
	for c in str.chars() {
		if is_pos_char(parser, c, &mut pos).is_err() {
			return false;
		}
	}
	parser.line_position = pos;
	true
}

fn is_pos_char(parser: &mut Parser, char: char, pos: &mut usize) -> Result<(), ()> {
	match parser.line_characters.get(*pos) {
		None => Err(()),
		Some(&c) => {
			if c == char {
				*pos += 1;
				Ok(())
			} else {
				Err(())
			}
		}
	}
}