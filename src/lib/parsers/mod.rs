mod char;

use super::model::parsed_item::ParsedItem;
use super::reader::file::File;
use std::{
    io::Lines,
    io::{self},
};
use super::error::{
    Error,
    ErrorKind,
};
use super::result::Result;

struct Parser<'a> {
	items: &'a mut Vec<ParsedItem>,
	line_string: &'a str,
	line_characters: Vec<char>,
	line_index: usize,
	line_position: usize,
}

pub fn parse(file: &str, items: &mut Vec<ParsedItem>) -> Result<()> {
    let mut parser = Parser {
		items,
		line_characters: Vec::new(),
		line_index: 0,
		line_string: "",
		line_position: 0,
    };

    match File::load(file) {
        Err(err) => return Err(err),
        Ok(file) => parser.parse_lines(&file.content)?
    };

    Ok(())
}

impl<'a> Parser<'a> {
    fn parse_lines(&mut self, content: &'a str) -> Result<()> {
		for (index, line) in content.lines().enumerate() {
			self.line_string = line.trim();
			self.line_characters = self.line_string.chars().collect();
			self.line_index = index;
            self.line_position = 0;
            // println!("{}", self.line_string);
			self.tokenize()?;
		}
		Ok(())
    }
    
    fn tokenize(&mut self) -> Result<()> {
		// char::readover_whitespaces(self);
		char::runner(self);
		// if chars:try_consume_char(self, |c| c == '\t') || chars::try_consume_string(self, "  ") {
		// 	chars::consume_whitespaces(self);
		// 	comment::tokenize_indented_comment(self)?;
		// 	posting::tokenize(self)?;
		// } else {
		// 	comment::tokenize_journal_comment(self)?;
		// 	transaction::tokenize(self)?;
		// 	include::tokenize(self)?;
		// 	// directives::is_alias(self)?;
		// }
		// if let Some(c) = self.line_characters.get(self.line_position) {
		// 	return Err(Error::LexerError(format!("unexpected character \"{}\"", c)));
		// }
		Ok(())
	}
}