mod lib;

use lib::parsers;

fn main() -> std::io::Result<()> {
    let mut items = Vec::new();
    parsers::parse("examples/sample1.ledger", &mut items)?;

    Ok(())
}
